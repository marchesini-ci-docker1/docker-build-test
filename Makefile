all: app test

app: src/main.o src/funzioni.o
	g++ -o app src/main.o src/funzioni.o

main.o: src/main.cpp src/funzioni.h
	g++ -c -o src/main.o src/main.cpp

funzioni.o: src/funzioni.cpp src/funzioni.h
	g++ -c -o src/funzioni.o funzioni.cpp

test: test/funzioni-test.o src/funzioni.o
	g++ -o test-funzioni test/funzioni-test.o src/funzioni.o

funzioni-test.o: test/funzioni-test.cpp src/funzioni.h
	g++ -o test/funzioni-test.o test/funzioni-test.cpp